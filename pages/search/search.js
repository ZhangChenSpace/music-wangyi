// pages/search/search.js
import request from '../../utils/request'
var time = null
Page({

  /**
   * 页面的初始数据
   */
  data: {
    defaultkeycode: "", //搜索框默认数据
    searchlist: [], //热搜榜数据
    inputvalue: '' ,//输入框实时的数据
    searchinfo:[],//模糊搜索出来的结果
    searchhistory:[]  //搜索历史
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.searchdefault()
    //如果本地搜索记录存在，那么就让他显示
    if(wx.getStorageSync('searchhistory')){
      this.setData({
        searchhistory:wx.getStorageSync('searchhistory')
      })
    }
    wx.removeStorageSync('searchhistory')
  },

  // 获取初始化数据
  async searchdefault() {
    let result = await request('/search/default')
    let result1 = await request('/search/hot/detail')
    this.setData({
      defaultkeycode: result.data.realkeyword,
      searchlist: result1.data
    })
  },

  // 实时获取input框的值
  getinput(event) {
    //这里的判断是当我们删除搜索框的内容时，删除刚刚搜索的结果，使得下一次进入搜索的时候，搜索结果为空白
    if(this.data.inputvalue==''){
      this.setData({
        searchinfo:[]
      })
    }
    this.setData({
      inputvalue: event.detail.value
    })
    setTimeout(() => {
      if (time) {
        clearTimeout(time)
      }
      time = setTimeout(() => {
        //这里，不仅仅是当我们手动清空搜索条的时候不发送一个keywords为空的请求，还得将data中的数据清除，不然在下一次数据来之前，你的搜索匹配框里面显示的都是上一次搜索的结果，这样的用户体验并不好
        if(this.data.inputvalue!=''){
          this.getdata(this.data.inputvalue,15)
        }
      }, 500);
    })
  },

  //模糊搜索出数据
  async getdata(keywords,limit) {
    let result=await request('/search',{keywords,limit})
    if(result.code==200){
      let {searchhistory}=this.data
      searchhistory.push(keywords)
      this.setData({
        searchinfo:result.result.songs,
        searchhistory
      })
    }
  },

  //删除搜索框的内容，不用手动去一下下的去清除\
  deletekeywords(){
    let {inputvalue}=this.data
    inputvalue=''
    this.setData({
      inputvalue
    })
  },

  //删除单个的搜索记录,主要方法就是在数组里面找到对应的下标，然后用splice方法，从原数组的temp下标位置，删除长度为1，就可以实现这个功能！
  deletesingle(event){
    let {searchhistory}=this.data
    let temp=searchhistory.findIndex(item=>{
      return item==event.currentTarget.id
    })
    searchhistory.splice(temp,1)
    this.setData({
      searchhistory
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    //当页面离开的时候，调用方法，将搜索数据本地化，以便下次onload的时候，直接将搜索记录加载到data中
    wx.setStorageSync('searchhistory', this.data.searchhistory)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})