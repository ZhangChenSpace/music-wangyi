// pages/recommendSong/recommendSong.js
import request from '../../../utils/request'
const allid=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    day:'',//几月几号
    month:'',
    songlist:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      day:new Date().getDay(),
      month:new Date().getMonth()
    })
    //判断用户是否登录
    if(wx.getStorageSync('usercookie')){
      //发送请求获取每日推荐数据
      this.getdayrecommendsong()
    }else{
      //没登录就跳转到登录页面
      wx.showToast({
        title: '请先登录',
        icon:'none',
        success:()=>{
          wx.reLaunch({
            url: '/pages/Login/Login',
          })
        }
      })
    }
  },

  //获取每日推荐歌曲
  async getdayrecommendsong(){
    let result=await request('/recommend/songs')
    //将id存入全局data中，方便其他页面使用
    let newarray=[]
    result.data.dailySongs.forEach(item=>{
        newarray.push(item.id)
    })
    allid.globalData.songs_all_id=newarray.slice(0,34)
    if(result.code==200){
      this.setData({
        songlist:result.data.dailySongs
      })
    }
  },

  //去歌曲详情页面
  GoSongDetail(event){
    wx.navigateTo({
      url: `../songDetail/songDetail?ids=${event.currentTarget.dataset.idd}&&name=${event.currentTarget.dataset.name}`
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})