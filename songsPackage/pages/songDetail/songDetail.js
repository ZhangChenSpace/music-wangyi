import request from '../../../utils/request'
import moment from 'moment'
const appinstance = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isplay: false, //控制音乐的播放还是暂停
    songinfo: [], //音乐的详情数据，不包括播放地址等信息
    songid: '', //歌曲的id
    songlength:'',//歌曲的总时长，但是毫秒为单位的
    realtime:'',//真实的播放时间
    pancentige:''//播放时间百分比
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      isplay: true
    })
    //获取歌曲详情
    this.getsonginfo(options.ids)
    //获取背景播放的实例
    this.backgroundMusicplay = wx.getBackgroundAudioManager()
    //监听音乐是否播放/暂停/停止
    this.backgroundMusicplay.onPlay(() => {
      this.setData({
        isplay: true
      })
      //设置全局的播放和id
      // appinstance.globalData.isMusicPlay=true
      appinstance.globalData.songId = options.ids
    })
    this.backgroundMusicplay.onPause(() => {
      this.setData({
        isplay: false
      })
      //全局设置播放暂停
      // appinstance.globalData.isMusicPlay=false
    })
    //监听歌曲是否自然播放完成
    this.backgroundMusicplay.onEnded(() => {
      //这里自己定义一个event,来模仿是用户点击了下一首，因为这里无法获取下一首的id的值
      let event={currentTarget:{id:'next'}}
      //在自然播放完成后，自动切换下一首
      this.changeSong(event)
    })

    //监听歌曲的实时进度
    this.backgroundMusicplay.onTimeUpdate(()=>{

      let realtime=moment(this.backgroundMusicplay.currentTime*1000).format('mm:ss')
      let pancentige=this.backgroundMusicplay.currentTime / this.data.songinfo[0].dt*1000*410
      this.setData({
        realtime,
        pancentige:pancentige+'rpx'
      })
    })
    //加载自动播放
    this.musiccontrol(options.ids, options.name)
  },
  //获取歌曲的具体详情（没有歌曲的长度那些信息）
  async getsonginfo(ids) {
    let result = await request('/song/detail', {
      ids: ids
    })
    let songlength=moment(result.songs[0].dt).format('mm:ss')
    if (result.code == 200) {
      this.setData({
        songinfo: result.songs,
        songid: ids,
        songlength
      })
    }
  },

  //点击底部的图标控制播放还是暂停
  changebofang() {
    let isplay = this.data.isplay
    this.setData({
      isplay: !isplay
    })
    //获取歌曲的长度等信息(我这里没有传入name参数，因为这里不是进入页面的自动播放，appdata里面已经有了songinfo信息，就不需要在使用路由里面的name属性的值了)
    this.musiccontrol(this.data.songid, this.data.songinfo[0].name)
  },

  //控制音乐播放/暂停的功能函数
  async musiccontrol(id, name) {
    let isplay = this.data.isplay
    //播放还是停止的判断
    if (isplay) {
      //这里判断，当前播放的音乐的名字，是不是和上一首音乐的名字一样，如果一样就不变，如何不一样，则播放最新音乐的名字
      if (appinstance.globalData.songname != name) {
        appinstance.globalData.songname = name
        let result = await request('/song/url', {
          id: id
        })
        if (result.code == 200) {
          this.backgroundMusicplay.src = result.data[0].url
          this.backgroundMusicplay.title = name
        }
        //如果歌曲名字一样，就表示触发这个播放的是点击播放和暂停图标，也就是继续播放咯
      } else {
        this.backgroundMusicplay.play()
      }
    } else {
      this.backgroundMusicplay.pause()
    }
  },
  //专门给下一首和上一首封装的一个函数
  async preornext(id){
    let result = await request('/song/detail', {
      ids: id
    })
    if (result.code == 200) {
      this.setData({
        songinfo: result.songs,
        songid: id
      })
      this.musiccontrol(id, result.songs[0].al.name)
    }
  },
  //上一首下一首的实现
  changeSong(event) {
    let songidnum = this.data.songid 
    let allid=appinstance.globalData.songs_all_id
    //获取当前歌曲在整个歌曲列表的下标
    let songid = allid.findIndex(item => {
      return item == songidnum
    })
    //在这里让歌曲暂停是为了页面的动画能够停止，这样看起来比较舒服
    if(this.backgroundMusicplay.play()){
      this.backgroundMusicplay.pause()
    }
    if (event.currentTarget.id == 'pre') {
      //判断当前歌曲是不是第一首或者最后一首，如果是第一首，就变成最后一首，达到循环播放列表的功能
      if(songid==0){
        songid=allid.length
      }
      this.preornext(allid[songid - 1])
    } else {
      if(songid==allid.length-1){
        songid=-1
      }
      this.preornext(allid[songid + 1])
    }
    this.backgroundMusicplay.play()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})