import request from "../../utils/request"
// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //轮播图下面的小点
    indicatorDots:true,
    //轮播图自动播放
    autoplay:true,
    //多长时间换图片
    interval:2000,
    //换图片动作的时长
    duration:1000,
    backgroundimgsrc:['/static/images/'],
    //轮播图
    imgbanner:[],
    //为你推荐
    RecommendMusic:[],
    //排行榜
    rankinglist:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
   onLoad: async function(options) {
     //初始页轮播图的接口
    let result1=await request('/banner',{type:2})
    this.setData({
      imgbanner:result1.banners
    })
    //精心推荐的接口
    let result2=await request('/personalized',{limit:8})
    this.setData({
      RecommendMusic:result2.result
    })
    //排行榜的接口
    let ranking_list=[]
    for(let index=0;index<5;index++){
      let result3=await request('/top/list',{idx:index})
      let rankingItem={name:result3.playlist.name,tracks:result3.playlist.tracks.slice(0,3)}
      ranking_list.push(rankingItem)
      this.setData({
      rankinglist:ranking_list
      })
    }
    
  },
  //去每日推荐详情页
  GoRecommendSong(){
    //判断是否登录
    if(wx.getStorageSync('userinfo')){
      wx.navigateTo({
        url: '/songsPackage/pages/recommendSong/recommendSong',
      })
    }else{
      wx.navigateTo({
        url: '/pages/Login/Login',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})