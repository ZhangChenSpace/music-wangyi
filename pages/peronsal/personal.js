import request from '../../utils/request'
let startY = 0 //手指开始触摸的位置
let moveY = 0 //手指移动到的位置
let movedistance = 0 //手指一共移动了多少距离
// pages/peronsal/personal.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    covertransform: 'translateY(0rpx)',
    covertransition: '',
    //给他设置一个默认的值，也就是说，当我们未登录的时候，页面应该有一个默认的样式，在登录过后，再去另外显示
    userinfo: {
      nickname: '游客',
      backgroundUrl:'/static/images/personal/bgImg2.jpg',
      avatarUrl:"/static/images/personal/missing-face.png"
    },
    recentPlay:[],
    musicList:{}
  },
  //用户未登录的时候，点击头像跳转到登录界面
  goLogin() {
    //这里直接通过判断用户的名字是否为默认值，来判断是否去进行页面的跳转
    if(this.data.userinfo.nickname=='游客'){
      wx.reLaunch({
        url: '/pages/Login/Login',
      })
    }else{
      //这里将来是做一下登录之后的操作，比如在登录之后，再点击头像，就跳转到退出登录的页面，而不是登录页面，等等之类的操作
    }
  },
  //下面三个方法是当前页面的移动的方法
  touchstart(event) {
    startY = event.touches[0].clientY
    this.setData({
      covertransition: ''
    })
  },
  touchmove(event) {
    moveY = event.touches[0].clientY
    movedistance = moveY - startY
    if (movedistance <= -80) {
      return
    }
    if (movedistance < 80) {
      this.setData({
        covertransform: `translateY(${movedistance}rpx)`
      })
    }
  },
  touchend() {
    movedistance = 0
    this.setData({
      covertransform: `translateY(${movedistance}rpx)`,
      covertransition: 'transform 0.3s linear'
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    try {
      let userinfo1 = JSON.parse(wx.getStorageSync('userinfo'))
      if (userinfo1) {
        this.setData({
          userinfo: userinfo1
        })
      }

    //获取播放记录的接口
    let result=request('/user/record',{uid:this.data.userinfo.userId,type:1})
    result.then((value)=>{
      this.setData({
        recentPlay:value.weekData
      })
    })

    } catch (error) {
      console.log(error)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})