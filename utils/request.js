// 发送ajax请求
/*
1.封装功能函数
  1.功能点明确
  2.函数内部应该保留固定代码(静态的)
  3.将动态的数据抽取成形参，由使用者根据自身的情况动态的传入实参(比如url地址，data)
  4.一个良好的功能函数，应该设置形参的默认值(ES6的形参默认值)
2.封装功能组件
  1.功能点明确
  2.函数内部应该保留固定代码(静态的)
  3.将动态的数据抽取成props参数，由使用者根据自身的情况以标签的属性的形式动态传入props数据
  4.一个良好的组件应该设置组件的必要性及数据类型
*/
import config from "./config"
export default(url,data,method='GET')=>{
  return new Promise((resolve,reject)=>{
    wx.request({
      url:config.testhost+url,
      data,
      method, // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      // 设置请求的 header
      header: {cookie:wx.getStorageSync('usercookie')[0]}, 
      success: function(res){
        // success
        if(url=="/user/detail"){
          //这里的cookie是从网易云音乐手动提取出来的，也不知道这个cookie会不会过期或者变化！！(经过测试，会变化，得定期更换，先清除localstoryge，再写入)
          wx.setStorageSync('usercookie',["MUSIC_U=0fc4540153f56be6e92acbd2e18ca37a9a02643578a84ad93dcd704bcba3bcc7519e07624a9f0053b7e91489449634a2b2ebdf6c907445a999b74b7a26fdc23fc4c805f5df5e0d82d4dbf082a8813684; Max-Age=1296000; Expires=Wed, 15 Mar 2023 08:50:40 GMT; Path=/; Domain=.music.163.com; HTTPOnly"])
        }
        resolve(res.data)
      },
      fail: function(err) {
        // fail
        reject(err)
      },
      complete: function() {
        // complete
      }
    })
  })
}