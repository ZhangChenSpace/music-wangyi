import request from '../../utils/request'
// pages/Login/Login.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone:'',
    password:''
  },
  userphone(){
    
  },
  userpassword(){
    
  },
  async login(){
    let phonereg=/^1[3-9]\d{9}$/
    let {phone,password}=this.data
    if(!phone){
      wx.showToast({
        title: '手机号不能为空!',
        icon:'error'
      })
      return 
    }else if(!phonereg.test(phone)){
      wx.showToast({
        title: '手机号输入错误!',
        icon:'error'
      })
      return 
    }else if(!password){
      wx.showToast({
        title: '密码不能为空',
        icon:'error'
      })
      return 
    }else{
      //这里的登录接口，因为网易的原因，暂时无法使用,所以直接使用了自己的userid，而不是通过电话和密码登录后获取用户信息
      let result=await request('/user/detail',{uid:8363814925})
      if(result.code===200){
        wx.showToast({
          title: '成功登录',
          icon:"success"
        })
        //将用户信息进行本地存储
        wx.setStorageSync('userinfo', JSON.stringify(result.profile))
        wx.switchTab({
          url: '/pages/peronsal/personal'
        })
      }else{
        wx.showToast({
          title: '好像出问题了，请再试一次吧',
          icon:'loading'
        })
      }
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})